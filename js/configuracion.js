// Configuración firebase



// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBWCVZ5HCjt-BEz9XPZWo3yCtGccYhL_QY",
    authDomain: "sitioweb74.firebaseapp.com",
    projectId: "sitioweb74",
    databaseURL: "https://sitioweb74-default-rtdb.firebaseio.com",
    storageBucket: "sitioweb74.appspot.com",
    messagingSenderId: "1002297494994",
    appId: "1:1002297494994:web:28472f0d68bcb9ef72cc0b"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();


// Declaración de objetos
var btnInsertar = document.getElementById('btnInsertar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
var btnTodos = document.getElementById('btnTodos');
var btnLimpiar = document.getElementById('btnLimpiar');
var btnDeshabilitar = document.getElementById('btnDeshabilitar');

var lista = document.getElementById('lista');
var contenidoProductos = document.getElementById('contenidoProductos');

var btnMostrarImagen = document.getElementById('verImagen');
var archivo = document.getElementById('archivo');



var ID = "";
var nombre = "";
var descripcion = "";
var precio = "";
var nombreIMG = "";
var url = "";
var estado = "";

if(window.location.href == "http://127.0.0.1:5500/html/productos.html"){
  window.onload = mostrarProductos();
}

function mostrarProductos(){

    const db = getDatabase();
    const dbRef = ref(db, 'productosExample');

    onValue(dbRef, (snapshot) => {
        if(lista){
            lista.innerHTML = "";
        }
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            if(lista){
                lista.innerHTML = lista.innerHTML + "<div class='card'> " + "<img src=' " + childData.url + "'> <h2>" + childData.nombre + " <br> (estado: " + childData.estado + ")</h2><p>"  + childData.descripcion + "</p><br><br> <p> Precio: $" + childData.precio+ " MXN</p>" + "<button>COMPRAR (id:"+ childKey + ")</button>";

            }else if(contenidoProductos){
                if(childData.estado == 0){
                    
                    contenidoProductos.innerHTML = contenidoProductos.innerHTML + "<div class='card'> " + "<img src=' " + childData.url + "'> <h2>" + childData.nombre + "</h2><p>"  + childData.descripcion + "</p><br><br> <p> Precio: $" + childData.precio+ " MXN</p>" + "<button>COMPRAR</button>";
                }else{

                }
                
            }
            

            // console.log(childKey + ":");
            // console.log(childData.nombre);
        });
    },{
        onlyOnce: true
    });

}


function leerInputs(){
    ID = document.getElementById('id').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    nombreIMG = document.getElementById('imgNombre').value;
    url = document.getElementById('url').value;
    estado = document.getElementById('status').value;

    // alert("Matricula: " + matricula + " Nombre: " + nombre + " Carrera: " + carrera + " Genero: " + genero);
}


async function insertarDatos(){

    await subirImagen();

    leerInputs();

   insertar();
}

async function insertar(){
    await set(ref(db,'productosExample/' + ID), {
        nombre: nombre,
        descripcion: descripcion,
        precio: precio,
        nombreIMG: nombreIMG,
        url: url,
        estado: estado

    }).then((response)=>{
        alert("Se agrego un registro con éxito");
    }).catch((error)=>{
        alert("Surgio un error: " + error);
    });
}

function mostrarDatos(){
    leerInputs();
    const dbref = ref(db);
    
    get(child(dbref,'productosExample/' + ID)).then((snapshot)=>{
        if (snapshot.exists()) {
            nombre = snapshot.val().nombre;
            descripcion = snapshot.val().descripcion;
            precio = snapshot.val().precio;
            nombreIMG = snapshot.val().nombreIMG;
            url = snapshot.val().url;
            estado = snapshot.val().estado;

            escribirInputs();
        }else{
            alert("No existe el producto");
        }
    }).catch((error)=>{
        alert("Surgió un error: " + error);
    });
}

function escribirInputs(){
    document.getElementById('id').value = ID;
    document.getElementById('nombre').value = nombre;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('precio').value = precio;
    document.getElementById('imgNombre').value = nombreIMG;
    document.getElementById('url').value = url;
    document.getElementById('status').value = estado;
}

async function actualizar(){


    await subirImagen();

    leerInputs();
    await conseguir();

    
    

}

function conseguir(){
    
        update(ref(db,'productosExample/'+ ID),{
            nombre:nombre,
            descripcion:descripcion,
            precio:precio,
            nombreIMG: nombreIMG,
            url: url,
            estado: estado
    
        }).then(()=>{
            alert("Se realizó la actualización");
            mostrarProductos();
        }).catch(()=>{
            alert("Surgió un error: " + error);
        });
    
}


function borrar(){

    leerInputs();

    if (ID == "") {
        alert("No hay ID que coincida con el producto");
        return;
    }else if(ID != null){
        remove(ref(db,'productosExample/'+ ID)).then(()=>{
            alert("Se borró el registro");
            mostrarProductos();
        }).catch(()=>{
            alert("Surgió un error" + error );
        });
    }

}

function limpiar(){

    lista.innerHTML="";
    ID = "";
    nombre = "";
    descripcion = "";
    precio = "";
    nombreIMG = "";
    url = "";
    estado = "0";

    escribirInputs();

}

var file = "";
var name = "";

// Permite cargar la imagen
function cargarImagen(){

    // archivo seleccionado
    file = event.target.files[0];
    name = event.target.files[0].name;
    document.getElementById('imgNombre').value = name;
}

// Llama la función cargar imagen y sube a la nube la imagen cargada
async function subirImagen(){
    // Sirve para subir la imagen al STORAGE
    const storage = getStorage();
    const storageRef = refS(storage, 'Imagenes/' + name);

    // 'file' comes from the Blob or File API
    await uploadBytes(storageRef, file).then((snapshot) => {
        alert("Se cargo el archivo");
    });

    await descargarImagen();

}

async function descargarImagen(){

    // import { getStorage, ref, getDownloadURL } from "firebase/storage";

    // Create a reference to the file we want to download
    const storage = getStorage();
    const storageRef = refS(storage, 'Imagenes/' + name);

    // Get the download URL
    await getDownloadURL(storageRef)
    .then((url) => {
        document.getElementById('url').value = url;
        document.getElementById('Preview').src = url;
    })
    .catch((error) => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
        case 'storage/object-not-found':
            console.log("No existe el archivo")
            break;
        case 'storage/unauthorized':
            console.log("No tiene permisos");
            break;
        case 'storage/canceled':
            console.log("Se canceló o no tiene internet");
            break;

        // ...

        case 'storage/unknown':
            console.log("Error desconocido");
            break;
        }
    });

}




// Evento click
if(btnInsertar){
    btnInsertar.addEventListener('click', insertarDatos);
}

if(btnBuscar){
    btnBuscar.addEventListener('click', mostrarDatos);
}

if(btnActualizar){
    btnActualizar.addEventListener('click', actualizar);
}

if(btnBorrar){
    btnBorrar.addEventListener('click', borrar);
}

if(btnTodos){
    btnTodos.addEventListener('click', mostrarProductos);
}

if(btnLimpiar){
    btnLimpiar.addEventListener('click', limpiar);
}

if(btnDeshabilitar){
    btnDeshabilitar.addEventListener('click', deshabilitar)
}

if(archivo){
    archivo.addEventListener('change', cargarImagen);
}


if(btnMostrarImagen){
    btnMostrarImagen.addEventListener('click', descargarImagen)
}

